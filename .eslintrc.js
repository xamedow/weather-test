module.exports = {
  extends: 'airbnb',
  "parser": "babel-eslint",
  "rules": {
    "strict": 0
  },
  env: {
    browser: true,
    node: true
  },

  settings: {
    'import/resolver': {
      webpack: {},
    },
  },
};
