import axios from 'axios';

export const API_KEY = '3f8211a6212e21b1b73f4a53b80224bf';
const WEATHER_API_URL = 'http://api.openweathermap.org/data/2.5/weather';

export const API = axios.create({
  baseURL: `${WEATHER_API_URL}`,
});

export const getLocation = (cb) => {
  if (!navigator.geolocation) {
    return null;
  }

  return navigator.geolocation.getCurrentPosition(cb);
};

export const kelvin2celcius = value => Math.round(value - 273.15);
