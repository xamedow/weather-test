import React from 'react';
import { observer } from 'mobx-react';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import StarIcon from '@material-ui/icons/Star';
import ListItemText from '@material-ui/core/ListItemText';

const City = observer(({ city, handleCityRemove }) => (
  <ListItem>
    {city.isDefault && <ListItemIcon>
      <StarIcon />
    </ListItemIcon>}
    <ListItemText
      inset
      primary={city.title}
      secondary={city.temp ? `температура сейчас: ${city.temp}℃` : null}
    />
    {!city.isDefault && <ListItemSecondaryAction>
      <IconButton aria-label="Delete" onClick={handleCityRemove(city.id)}>
        <DeleteIcon />
      </IconButton>
    </ListItemSecondaryAction>}
  </ListItem>
));

export default City;
