import React from "react";
import { observable, action } from "mobx";
import { observer } from "mobx-react";
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import City from "./City";
import { API, API_KEY, getLocation } from '../utils';

const styles = theme => ({
  list: {
    maxWidth: 460,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  listWrapper: {
    position: 'relative',
    maxWidth: 460
  },
  progressWrapper: {
    position: 'absolute',
    left: 0,
    top: 0,
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, .8)',
    zIndex: 5,
  },
  progress: {
    margin: `${theme.spacing.unit * 2}`,
  }
});

@observer
class CitiesList extends React.Component {
  @observable newCityTitle = "";
  @observable isLoading = false;
  @observable error = null;

  componentDidMount() {
    this.isLoading = true;
    getLocation(this.getCurrentCityWheather);
  }

  fetchCity = (url, isDefault) => {
    API
      .get(url)
      .then((response) => {
        if (response.data) {
          this.props.store.addCity({ isDefault, ...response.data });
          this.newCityTitle = "";
        } else {
          throw new Error('No city found.');
        }
        this.isLoading = false;
      })
      .catch(() => {
        this.error = true;
        this.isLoading = false;
      });
  };

  getCurrentCityWheather = (position) => {
    if(position && position.coords.latitude && position.coords.longitude) {
      const { latitude: lat, longitude: lon } = position.coords;
      this.fetchCity(`?APPID=${API_KEY}&lat=${lat}&lon=${lon}`, true);
    }
  };

  render () {
    const { classes } = this.props;

    return (
      <div>
        <form onSubmit={this.handleFormSubmit}>
          <TextField
            label="Новый город:"
            className={classes.textField}
            value={this.newCityTitle}
            onChange={this.handleInputChange}
            margin="normal"
            error={this.error}
            helperText={this.error && 'Город не найден'}
          />
          <Button color="primary" className={classes.button} type="submit">
            Добавить
          </Button>
        </form>

        <Grid container spacing={16}>
          <Grid item xs={12} md={6} className={classes.listWrapper}>
            {
              this.isLoading &&
              <div className={classes.progressWrapper}>
                <CircularProgress className={classes.progress} />
              </div>
            }
            <List className={classes.list}>
              {this.props.store.cities.map(city => (<City
                city={city}
                key={city.id}
                handleCityRemove={this.handleCityRemove}
              />))}
            </List>
          </Grid>
        </Grid>
      </div>
    );
  }

  @action
  handleInputChange = e => {
    this.newCityTitle = e.target.value;
  };

  @action
  handleFormSubmit = e => {
    this.isLoading = true;
    this.error = null;
    if (this.newCityTitle) {
      this.fetchCity(`?APPID=${API_KEY}&q=${this.newCityTitle}`);
    }
    e.preventDefault();
  };

  @action
  handleCityRemove = (cityId) => () => {
    this.props.store.removeCity(cityId);
  }
}

export default withStyles(styles)(CitiesList);
