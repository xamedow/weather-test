import React from 'react';
import { render } from 'react-dom';
import DevTools from 'mobx-react-devtools';

import CitiesList from './components/CitiesList';
import CitiesListModel from './models/CitiesListModel';

const store = new CitiesListModel();

render(
  <div>
    <DevTools />
    <CitiesList store={store} />
  </div>,
  document.getElementById('root'),
);

// playing around in the console
window.store = store;
