import { observable } from 'mobx';
import { kelvin2celcius } from '../utils';

export default class CityModel {
  id = Math.ceil(Math.random() * 1000);
  @observable title;
  @observable temp;
  @observable isDefault;

  constructor(data) {
    this.title = data.name;
    this.isDefault = data.isDefault;
    this.temp = kelvin2celcius(data.main.temp);
  }
}
