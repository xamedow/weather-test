import { observable, action } from 'mobx';
import CityModel from './CityModel';

export default class CitiesListModel {
  @observable cities = [];

  @action
  addCity(data) {
    this.cities.push(new CityModel(data));
    window.sessionStorage.setItem('cities', JSON.stringify(this.cities));
  }

  @action
  removeCity(id) {
    this.cities = this.cities.filter(item => item.id !== id);
    window.sessionStorage.setItem('cities', JSON.stringify(this.cities));
  }
}
