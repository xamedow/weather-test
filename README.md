Weather test app
=====================

A test weather application.
Supports ES6 and JSX compilation through babel.


### Run the app

```
npm install
npm start
```

Browser should open automatically. Otherwise, navigate to the URL reported in the terminal
